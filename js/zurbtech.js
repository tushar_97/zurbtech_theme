/**
 * @file
 * Theme hooks for the Drupal Bootstrap base theme.
 */
(function ($, Drupal, zurbtech, Attributes) {

  $('.slick-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
   
  });

})(window.jQuery, window.Drupal, window.Drupal.zurbtech, window.Attributes);



